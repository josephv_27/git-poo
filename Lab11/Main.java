package laboratorio11;
import java.io.IOException;
import java.util.Scanner;
import java.io.*;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import SupermercadoProfe.*;


public class Main {

	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub
		Supermercado SUPERMARKET = new Supermercado("SUPERMARKET");
		AdministradorArchivos TablaProductos = new AdministradorArchivos();
		
		System.out.println("----- Estos son los productos populares -----");
		
		// imprimir los productos del xls importado
		
		for(int i = 0; i<TablaProductos.filas; i++) {
			
			Cell CeldaConProducto = TablaProductos.HojaDeProductos.getCell(0,i);
			  
			System.out.println(CeldaConProducto.getContents());
			
		}
		
		System.out.println("----- Desea Agregar Membresias al supermercado "+ SUPERMARKET.getNombre()+ "-------");
		System.out.println("Digite [Y/N]");
		Scanner scan = new Scanner(System.in);
		String opcionMemb = scan.next();

		switch (opcionMemb)
		{
		     case "Y":
		    	 System.out.println("Cuantas membresias quiere agregar?");
		    	 int membresias = scan.nextInt();
		    	 for(int  i = 0; i <membresias; i++) {
			    	 System.out.println("Cual es el nombre de la membresia?");
			    	 String nombreMemb = scan.next();
			    	 System.out.println("Cuanto es el descuento de la membresia");
			    	 float descuentoMemb = scan.nextFloat();
		    		 SUPERMARKET.agregarMembresia(nombreMemb, descuentoMemb);
		    	 }
		     //Java code
		     break;
		     case "N":
		 		System.out.println("No se han agregado membresias");

		     //Java code
		     break; 
		     default:
		    	System.out.println("No se han agregado membresias");
		     //Java code
		}
		System.out.println("----- Desea Agregar Promociones al supermercado "+ SUPERMARKET.getNombre()+ "-------");
		System.out.println("Digite [Y/N]");
		
		String opcionPromo;
		opcionPromo = scan.next();
		switch (opcionPromo)
		{
		     case "Y":
		    	 System.out.println("Cuantas promociones desea agregar?");
		    	 int promociones = scan.nextInt();
		    	 for(int  i = 0; i <promociones; i++) {
			    	 System.out.println("Cual es la promocion?");
			    	 String nombrePromo = scan.next();
			    	 System.out.println("Cual nombre del producto que incluye la promocion");
			    	 String nombreProducto = scan.next();
			    	 
			    	 Producto productoNuevo = new Producto(nombreProducto);
			    	 productoNuevo.setPromocion(nombrePromo, 5, (float)0.00);
		    		 SUPERMARKET.agregarProductoInventarioVentaUnitaria(productoNuevo);
		    	 }
		     //Java code
		     break;
		     case "N":
		 		System.out.println("No se han agregado promociones");

		     //Java code
		     break; 
		     default:
		    	System.out.println("No se han agregado promociones");
		     //Java code
		}
		SUPERMARKET.imprimirMembresias();
		SUPERMARKET.imprimirInventario();
	}
}


